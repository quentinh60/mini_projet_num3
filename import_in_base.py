import logging
import sqlite3
import sys
import csv
import os
import atexit

def connection_close(connection):
    if connection:
        connection.close()

def connect(fichier_sql):
    connection = sqlite3.connect(fichier_sql)
    return connection

def get_csv(fichier_csv, delimiteur):
    with open(fichier_csv, 'r') as csvfile: 
        reader = csv.reader(csvfile, delimiter=delimiteur)
        donnees = []
        for row in reader:
            donnees.append(row)
    return(donnees)

def verif_create_base(fichier_sql):
    if(os.path.exists(fichier_sql)):
        logging.info('La base de donnees existe')
    else: 
        logging.debug("La base de donnees n'existe pas -> creation")
    connection = connect(fichier_sql)
    connection.commit()
    connection_close(connection)

def verif_create_table(fichier_sql):
    connection = connect(fichier_sql)
    cursor = connection.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    if(cursor.fetchall() != []):
        logging.info('La table existe')
    else :
        logging.debug("La table n'existe pas -> creation")
        commande = open("createtable.txt", 'r')
        cursor.execute(commande.read())
        commande.close()
        logging.info('La table est creee')
    connection.commit()
    cursor.close()
    connection_close(connection)

def insert_in_table(fichier_sql, data):
    del data[0]
    connection = connect(fichier_sql)
    cursor = connection.cursor()
    insertion = 0
    update = 0
    id = 0
    for row in data:
        id += 1
        ligne = str(id)
        data_immatriculation = row[3]
        cursor.execute("SELECT * FROM SIV WHERE immatriculation=?",(data_immatriculation,))
        if(cursor.fetchall() == []):
            cursor.execute("INSERT OR IGNORE INTO SIV VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", (ligne, row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18]))
            insertion += 1
        else:
            cursor.execute('''UPDATE SIV SET adresse_titulaire= ?, nom= ?,prenom= ?, date_immatriculation= ?, vin= ?, marque= ?, denomination_commerciale= ?, couleur= ?, carroserie= ?, categorie= ?, cylindree= ?, energie= ?, places= ?, poids= ?, puissance= ?, type= ?, variante= ?, version= ? WHERE immatriculation = ?
            ''', (row[0], row[1], row[2], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18], data_immatriculation))  
            update += 1
    logging.info('%d donnees ont ete inseres et/ou %d ont ete mises a jour', insertion, update)
    connection.commit()
    cursor.close()
    connection_close(connection)


def main(fichier_sql, fichier_csv):
    logging.basicConfig(filename='fichier_logging.log', level=logging.DEBUG, format='%(asctime)s: %(message)s')
    logging.info('Started')
    donnees = get_csv(fichier_csv, ';')
    verif_create_base(fichier_sql)
    verif_create_table(fichier_sql)
    insert_in_table(fichier_sql, donnees)
    logging.info('Finished')

if __name__ == '__main__':
    main("csvbase.sqlite3", "newfichier_automobile.csv")