from import_in_base import *
import unittest

class Testimport(unittest.TestCase):

    def test_exist_table(self):
        connection = connect("csvbase.sqlite3")
        cursor = connection.cursor()
        commande = open("testcreatetable.txt", 'r')
        cursor.execute(commande.read())
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        self.assertEqual(cursor.fetchall(),[('SIV',), ('test',)])
        commande.close()
        connection.commit()
        cursor.close()
        connection_close(connection)


    def test_insertion_valeurs(self):
        connection = connect("csvbase.sqlite3")
        cursor = connection.cursor()
        cursor.execute("INSERT INTO test VALUES ('1', 'Tontouchet', 'Meddy', 'bk-529-jg')")
        cursor.execute("SELECT * FROM test;")
        self.assertEqual(cursor.fetchone(), (1, 'Tontouchet', 'Meddy', 'bk-529-jg'))
        connection.commit()
        cursor.close()
        connection_close(connection)


    def test_exist_base(self):
        self.assertEqual(os.path.exists("csvbase.sqlite3"), True)
        self.assertEqual(os.path.exists("bidule.sqlite3"), False)

    def test_mise_a_jour(self):
        connection = connect("csvbase.sqlite3")
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM test WHERE immatriculation=?;",('bk-529-jg',))
        if(cursor.fetchall() != []):
            cursor.execute("UPDATE test SET id='1', nom='Montouchet', prenom='Teddy'")
        cursor.execute("SELECT prenom FROM test")
        self.assertEqual(cursor.fetchone(), ('Teddy',))
        cursor.execute("DROP TABLE test")
        connection.commit()
        cursor.close()
        connection_close(connection)

    